#include <stdio.h>
#include <pthread.h>
#include <cstdlib>
#include <string.h>

double daFunc(double x, double y);
double megaComputation(int fragmentCount);
void *computationThread(void* someArg);
double parallCompute(int fragmentCount);

/*
*	Program entry point
*/
int main(int argc, char *argv[]) {
	int exitCode = 1;
	printf("Welcome to lab1\n");
	if(argc >= 2) {
		int splitN = atoi(argv[1]);
		printf("Got something from you: N=%i\n", splitN);
		double result = 0.0;
		if(argc == 2) {
			result = megaComputation(splitN);
			printf("Got your result: %E\n", result);
		} else if(argc == 3 || strcmp(argv[2], "parall") == 0) {
			result = parallCompute(splitN);
			printf("Got your result: %E\n", result);
		} else {
			printf("You're doing it wrong. Do it this way:\n\t%s ARG\n [parall]", argv[0]);
		}
		exitCode = 0;
	} else {
		printf("I bet you'd like to specify some arguments, my pal. Do it this way:\n\t%s ARG\n [parall]", argv[0]);
	}
	return exitCode;
}

/*
*	Function that limits the prism
*/
double daFunc(double x, double y) {
	return x*x + y;
}

/*
*	Serial computation function
*/
double megaComputation(int fragmentCount) {
	double sideLength = 1.0;
	double hStep = sideLength / fragmentCount;
	double resultBuf = 0;
	for(int i = 0; i < fragmentCount; ++i) {
		for(int j = 0; j <= i; ++j) {
			double argX = (fragmentCount - i - 0.5) * hStep;
			double argY = (j + 0.5) * hStep;
			double multiplier = hStep * hStep;
			double val = daFunc(argX, argY) * ((j != i) ? multiplier : multiplier / 2.0);
			printf("%E\n", val);
			resultBuf += val;
		}
	}
	return resultBuf;
}

/*
*	Struct for storing arguments that will be passed to a created thread
*/
struct pthreadArgs {
	int iBegin, shift, fragmentCount;
	double hStep;
	pthread_cond_t condvar;
};

/*
*	This function is to be launched in a separate thread
*/
void *computationThread(void* someArg) {
	//getting arguments, saving them to local variables
	pthreadArgs* args = (pthreadArgs*) someArg;

	int iBegin = args->iBegin;
	int shift = args->shift;
	double hStep = args->hStep;
	int fragmentCount = args->fragmentCount;

	//arguments have been read. Now we can send signal to main thread
	pthread_cond_signal(&args->condvar);
	

	double *resultBuf = new double();
	*resultBuf = 0.0;
	for(int i = iBegin; i < fragmentCount; i += shift) {
		for(int j = 0; j <= i; ++j) {
			double argX = (fragmentCount - i - 0.5) * hStep;
			double argY = (j + 0.5) * hStep;
			double multiplier = hStep * hStep;
			double val = daFunc(argX, argY) * ((j != i) ? multiplier : multiplier / 2.0);
			printf("Thread %d: %E\n", iBegin, val);
			*resultBuf += val;
		}
	}
	pthread_exit((void *) resultBuf);
}

/*
*	Computing the same function, now in parallel manner.
*	Number of threads is explicitly specified inside
*/
double parallCompute(int fragmentCount) {
	//hard-code this parameter
	double sideLength = 1.0;
	double hStep = sideLength / fragmentCount;
	double resultBuf = 0.0;
	double finalResult = 0.0;
	int returnCode = 0;
	int numOfThreads = 4;
	pthread_mutex_t mutex;

	//we'll use one argument struct for all the threads
	pthreadArgs argBuf;

	//we shall need this wonderful mutex and conditional variable
	//to suspend main thread's execution until thread reads all the data
	pthread_mutex_init(&mutex, NULL);
	pthread_cond_init(&argBuf.condvar, NULL);

	//these values are shared between all threads
	argBuf.hStep = hStep;
	argBuf.fragmentCount = fragmentCount;
	argBuf.shift = numOfThreads;

	pthread_t threadArr[numOfThreads];

	//now the show begins
	//creating threads
	int threadId = 0;
	for(threadId = 0; threadId < numOfThreads; ++threadId) {
		pthread_mutex_lock(&mutex);
		argBuf.iBegin = threadId;

		returnCode = pthread_create(&threadArr[threadId], NULL, computationThread, &argBuf);

		if(returnCode != 0) {
			printf("All hope is gone! Thread number %d returned code %d", threadId, returnCode);
			exit(1);
		}

		pthread_cond_wait(&argBuf.condvar, &mutex);
		pthread_mutex_unlock(&mutex);
	}

	//joining threads to get results
	for(threadId = 0; threadId < numOfThreads; ++threadId) {
		void *threadOutput;

		returnCode = pthread_join(threadArr[threadId], &threadOutput);
		if(returnCode != 0) {
			printf("All hope is gone! Thread number %d returned code %d", threadId, returnCode);
			exit(1);
		}

		resultBuf = *(double *) threadOutput;
		delete((double*)threadOutput);
		finalResult += resultBuf;
	}
	pthread_mutex_destroy(&mutex);
	return finalResult;
}