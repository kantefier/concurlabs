#include <iostream>
#include <stdlib.h>
#include <math.h>
#include "mpi.h"
#include "DoubleMatrix.h"
using namespace std;

void debugMatrixPrint(DoubleMatrix *a, DoubleMatrix *b, DoubleMatrix *c, int rank);
int packMatrix(DoubleMatrix &matr, char *&outBuffer, int *position);
DoubleMatrix unpackMatrix(void *inBuffer, int buffSize);
double HartleyKoeff(int i, int j, int n);

/**
*	Program entry point
*	Arguments:е
*		~ argv[1]: matrix size
*	Example:
*		./myprog 5
*/
int main(int argc, char *argv[]) {
	//matrices are square-sized, i.e. matrSize x matrSize
	int matrSize = 0;
	int procCount, rank;

	//starting parallel work
	MPI_Init(&argc, &argv);

	MPI_Comm_size(MPI_COMM_WORLD, &procCount);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	//checking for input errors
	if(argc == 2) {
		matrSize = atoi(argv[1]);
		
		if(matrSize < procCount) {
			if(rank == 0) {
				cout << "Fatal Error: [matrix size < number of processes]" << endl;
				cout << "Description: matrix size should be bigger than number of processes" << endl;
			}
			MPI_Finalize();
			return 1;
		}
	} else {
		if(rank == 0)
			cout << "Unacceptable!!!!11 Program should have one argument - the size of matrix" << endl;
		MPI_Finalize();
		return 1;
	}

	//every process should know, what chunks of matrices it should compute
	int baseChunkSize = matrSize / procCount;

	//remaining rows count
	int remainingRows = matrSize % procCount;
	int mAChunkSize;
	
	if(remainingRows - rank > 0) {
		//increase chunk size for this process by one
		mAChunkSize = baseChunkSize + 1;
	} else {
		//no remainders left fot this process
		mAChunkSize = baseChunkSize;
	}

	//counting how many rows are owned by previous processes
	int prevChunksSum = 0;
	for(int procId = 0; procId < rank; ++procId) {
		if(remainingRows - procId > 0)
			prevChunksSum += baseChunkSize + 1;
		else
			prevChunksSum += baseChunkSize;
	}
	//determine what range of rows of matrix A this process owns
	int iChunkStart = prevChunksSum;
	int iChunkEnd = iChunkStart + mAChunkSize - 1;

	//Debugging
	cout << "Rank" << rank << ": range: [" << iChunkStart << ", " << iChunkEnd << "]" << endl;

	DoubleMatrix matrAChunk = DoubleMatrix(mAChunkSize, matrSize);
	DoubleMatrix matrBChunk = DoubleMatrix(matrSize, mAChunkSize);

	//generate chunks of matrices
	//every process generates his own chunks of data
	for(int i = 0; i < matrAChunk.getRowCount(); ++i) {
		for(int j = 0; j < matrAChunk.getColCount(); ++j) {
			matrAChunk(i, j) = HartleyKoeff(iChunkStart + i, j, matrSize);
		}
	}
	for(int i = 0; i < matrBChunk.getRowCount(); ++i) {
		for(int j = 0; j < matrBChunk.getColCount(); ++j) {
			matrBChunk(i, j) = HartleyKoeff(i, iChunkStart + j, matrSize) / matrSize;
		}
	}

	//matrix for results
	DoubleMatrix resultChunk = DoubleMatrix();
	DoubleMatrix leftChunk = DoubleMatrix();
	DoubleMatrix tempMatrix = DoubleMatrix();
	int iter;
	char *outBuffer = NULL;
	char *inBuffer = NULL;
	int outPosition = 0;
	int sourceRank, destinationRank;
	if(rank == 0) {
		destinationRank = procCount - 1;
		sourceRank = rank + 1;
	} else if(rank + 1 == procCount) {
		destinationRank = rank - 1;
		sourceRank = 0;
	} else {
		destinationRank = rank - 1;
		sourceRank = rank + 1;
	}

	
	//iterations start
	for(iter = 1; iter <= procCount; ++iter) {
		if(iter == 1) {
			//on first iteration we're using our generated matrices
			resultChunk = matrAChunk * matrBChunk;
		} else if(iter <= procCount - rank) {
			//that means that we are getting matrices from the right side
			tempMatrix = matrAChunk * matrBChunk;
			resultChunk = DoubleMatrix::concatHorizontal(resultChunk, tempMatrix);
		} else if(iter < procCount) {
			//that means that we are getting matrices from the left border, not neighboring with our results
			//save results without concatenation to final results
			if(leftChunk.getRowCount() == 0) {
				//means it's empty now. Just init
				leftChunk = matrAChunk * matrBChunk;
			} else {
				//not empty, concat
				tempMatrix = matrAChunk * matrBChunk;
				leftChunk = DoubleMatrix::concatHorizontal(leftChunk, tempMatrix);
			}
		} else if(iter == procCount) {
			//last iteration, concatenate left chunk
			if(leftChunk.getRowCount() == 0) {
				//means it's empty now. Just init
				leftChunk = matrAChunk * matrBChunk;
			} else {
				//not empty, concat
				tempMatrix = matrAChunk * matrBChunk;
				leftChunk = DoubleMatrix::concatHorizontal(leftChunk, tempMatrix);
			}
			resultChunk = DoubleMatrix::concatHorizontal(leftChunk, resultChunk);
			// break;
		}

		//not the last iteration
		if(iter != procCount) {
			//non-blocking send
			int sendBuffSize = packMatrix(matrBChunk, outBuffer, &outPosition);
			MPI_Request request;
			MPI_Isend(outBuffer,
				sendBuffSize,
				MPI_PACKED,
				destinationRank,
				0,
				MPI_COMM_WORLD,
				&request);

			//blocking receive
			MPI_Status status;			
			int recvBuffSize = 0;
			MPI_Probe(sourceRank, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			MPI_Get_count(&status, MPI_PACKED, &recvBuffSize);

			inBuffer = new char[recvBuffSize]();
			MPI_Recv(inBuffer, recvBuffSize, MPI_PACKED, sourceRank, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			matrBChunk = unpackMatrix((void*) inBuffer, recvBuffSize);

			//cleanup
			delete(inBuffer);

			MPI_Wait(&request, MPI_STATUS_IGNORE);
		}
	}

	MPI_Barrier(MPI_COMM_WORLD);
	if(rank == 0)
		cout << "Barrier reached!" << endl;
	//when iterations are over
	//you've got no place to go
	//well then, send the process NIL your fucking results, please
	if(rank != 0) {
		int sendBuffSize = packMatrix(resultChunk, outBuffer, &outPosition);
		MPI_Send(outBuffer, sendBuffSize, MPI_PACKED, 0, 0, MPI_COMM_WORLD);
	} else {
		for(int procId = 1; procId < procCount; ++procId) {
			MPI_Status status;
			int recvBuffSize = 0;
			MPI_Probe(procId, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			MPI_Get_count(&status, MPI_PACKED, &recvBuffSize);

			inBuffer = new char[recvBuffSize]();
			MPI_Recv(inBuffer, recvBuffSize, MPI_PACKED, procId, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			tempMatrix = unpackMatrix((void*) inBuffer, recvBuffSize);
			resultChunk = DoubleMatrix::concatVertical(resultChunk, tempMatrix);
		}
	}

	//when the music's over
	//display the matrix from Nil process
	//and turn off the lights :)
	if(rank == 0)
		resultChunk.display();
	MPI_Finalize();
	return 0;
}

/**
*	Function for Hartley coefficients
*/
double HartleyKoeff(int i, int j, int n) {
	return (double) M_SQRT2 * cos( (2.0 * M_PIl * i * j) / n - (double) M_PI_4l);
}

/**
*	Packing matrix into char* buffer and int* position for mpi_send
*/
int packMatrix(DoubleMatrix &matr, char *&outBuffer, int *position) {
	int outBuffSize = 0;
	int tempSizeOut = 0;
	//counting the size of output buffer
	MPI_Pack_size(1, MPI_INT, MPI_COMM_WORLD, &tempSizeOut);
	outBuffSize += tempSizeOut * 2;
	MPI_Pack_size(matr.getRowCount() * matr.getColCount(), MPI_DOUBLE, MPI_COMM_WORLD, &tempSizeOut);
	outBuffSize += tempSizeOut;

	delete(outBuffer);
	outBuffer = new char[outBuffSize]();

	int matrRowCount = matr.getRowCount();
	int matrColCount = matr.getColCount();
	int matrDims[] = {matrRowCount, matrColCount};
	*position = 0;

	MPI_Pack(matrDims,
		2,
		MPI_INT,
		outBuffer,
		outBuffSize,
		position,
		MPI_COMM_WORLD);

	double matrArr[matrRowCount * matrColCount];
	for(int i = 0; i < matrRowCount * matrColCount; ++i) {
		matrArr[i] = matr(i / matrColCount, i % matrColCount);
	}
	MPI_Pack(matrArr,
		matrRowCount * matrColCount,
		MPI_DOUBLE,
		outBuffer,
		outBuffSize,
		position,
		MPI_COMM_WORLD);

	return outBuffSize;
}

/**
*	Unpacking matrix from char* inBuffer with int buffSize
*/
DoubleMatrix unpackMatrix(void *inBuffer, int buffSize) {
	//vars to store extracted data
	int matrDims[2];
	double *matrValsBuff = NULL;

	int position = 0;
	//getting matrix dimensions
	MPI_Unpack(inBuffer,
		buffSize,
		&position,
		matrDims,
		2,
		MPI_INT,
		MPI_COMM_WORLD);

	int numOfValues = matrDims[0] * matrDims[1];
	matrValsBuff = new double[numOfValues]();

	//getting all the values
	MPI_Unpack(inBuffer,
		buffSize,
		&position,
		matrValsBuff,
		numOfValues,
		MPI_DOUBLE,
		MPI_COMM_WORLD);

	DoubleMatrix resultM = DoubleMatrix(matrDims[0], matrDims[1]);
	for(int i = 0; i < resultM.getRowCount(); ++i) {
		for(int j = 0; j < resultM.getColCount(); ++j) {
			resultM(i, j) = matrValsBuff[i * matrDims[1] + j];
		}
	}

	delete(matrValsBuff);

	return resultM;
}

/**
*	Debugging function
*/
void debugMatrixPrint(DoubleMatrix *a, DoubleMatrix *b, DoubleMatrix *c, int rank) {
	cout << "Rank" << rank << ": matrix A:" << endl;
	a->display();
	cout << endl;
	cout << "Rank" << rank << ": matrix B:" << endl;
	b->display();
	cout << endl;
	cout << "Rank" << rank << ": matrix C:" << endl;
	c->display();
	cout << endl;
	cout << endl;
}