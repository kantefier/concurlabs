#include <iostream>
#include <cstdlib>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <cstring>
#include <string>
using namespace std;

int serverMode(int portno, int numOfClients, int numOfFragments);
int clientMode(string hostname, int portno);
double daFunc(double x, double y);
void quitWithError(string errMessage, int exitCode = 1);

/*
*	Struct for storing arguments that will be passed to client
*/
struct dataForClient {
	int iBegin, shift, fragmentCount;
	double hStep;
};

/**
*	Program entry point
*/
int main(int argc, char *argv[]) {
	int portno, result, numOfClients, fragmentCount;
	string hostname;
	if(argc == 5 && string(argv[1]) == "server") {
		//Here comes the Server
		cout << "Welcome to the server mode\n";
		portno = atoi(argv[2]);
		numOfClients = atoi(argv[3]);
		fragmentCount = atoi(argv[4]);
		cout << "Starting server at port " << portno << " waiting for " << numOfClients << " clients to begin" << endl;
		result = serverMode(portno, numOfClients, fragmentCount);
	} else if(argc == 4 && string(argv[1]) == "client") {
		//And there's the client
		cout << "Welcome to the client mode" << endl;
		hostname = argv[2];
		portno = atoi(argv[3]);
		cout << "Connecting to server:\n\tHost: " << hostname << "\n\tport: " << portno << endl;
		result = clientMode(hostname, portno);
	} else {
		cout << "Not enough arguments.\n";
		cout << "\tYou should specify either \"server [port number] [numberOfClients] [numberOfFragments]\" or \"client [hostname] [port]\"\n";
		result = 1;
	}
	return result;
}

/**
*	Server side
*/
int serverMode(int portno, int numOfClients, int numOfFragments) {
	//vars for computation
	double sideLength = 1.0;
	double hStep = sideLength / numOfFragments;

	//socket variables etc.
	int sockfd, newsockfd;
	struct sockaddr_in serv_addr, cli_addr;
	int clientSock[numOfClients];
	dataForClient clientData[numOfClients];

	//create socket
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd < 0) {
		cerr << "Error opening socket";
		return 1;
	}
	serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);

    //bind socket to port
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
    	cerr << "Error binding socket to port " << portno << endl;
    	return 1;
    }

    //listen to socket with queue length 5 (MAX)
    listen(sockfd, 5);

    int i = 0;
    int returnCode = 0;
    //accept connection from clients one by one. Blocking function
    for(i = 0; i < numOfClients; ++i) {
    	socklen_t clilen = sizeof(cli_addr);
    	clientSock[i] = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
    	if(clientSock[i] < 0) {
    		quitWithError("Error on Accept for client #" + i, 1);
    	} else {
    		cout << "Accepted connection to client #" << i << endl;
    	}
    }

    //clients are waiting for all the other clients to make connection
    //once we've established connection to all the clients
    //we can start to send assignments to them
    for(i = 0; i < numOfClients; ++i) {
    	//prepare data
    	clientData[i].fragmentCount = numOfFragments;
    	clientData[i].hStep = hStep;
    	clientData[i].shift = numOfClients;
    	clientData[i].iBegin = i;
    	//send data to client
    	returnCode = write(clientSock[i], (void *) &clientData[i], sizeof(clientData[i]));
    	if(returnCode < 0)
    		quitWithError(string("Error writing data to client #" + i), 1);
    }

    double resultBuf = 0.0;
    double overallResult = 0.0;

    for(i = 0; i < numOfClients; ++i) {
    	int returnCode = recv(clientSock[i], (void *) &resultBuf, sizeof(double), 0);
    	if(returnCode < 0)
    		quitWithError("Error reading from client #" + i, 1);
    	overallResult += resultBuf;
    	close(clientSock[i]);
    }
    close(sockfd);
    cout << "Result: " << overallResult;
    return 0;
}

/**
*	Client side
*/
int clientMode(string hostname, int portno) {
	int sockfd;
	int returnCode = 0;
	struct sockaddr_in serv_addr;
    struct hostent *server;
    dataForClient *recvDataBuff = new dataForClient();

    //create socket
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        quitWithError("Error opening socket", 1);
    //find host
    server = gethostbyname(hostname.c_str());
    if (server == NULL) {
        quitWithError("Error: no such host", 1);
    }

    // bzero((char *) &serv_addr, sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    // memcpy((char *) server->h_addr, (char *) &serv_addr.sin_addr.s_addr, server->h_length);
    //IGNORE HOSTNAME
    serv_addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    // serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    serv_addr.sin_port = htons(portno);

    //connect to server
    if (connect(sockfd,(struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
        quitWithError("Error on connect", 1);

    //get data from server
    returnCode = recv(sockfd, recvDataBuff, sizeof(dataForClient), 0);
    if(returnCode < 0)
    	quitWithError("Error reading from socket", returnCode);

    int iBegin = recvDataBuff->iBegin;
    int shift = recvDataBuff->shift;
    int fragmentCount = recvDataBuff->fragmentCount;
    double hStep = recvDataBuff->hStep;

	double resultBuf = 0.0;
	for(int i = iBegin; i < fragmentCount; i += shift) {
		for(int j = 0; j <= i; ++j) {
			double argX = (fragmentCount - i - 0.5) * hStep;
			double argY = (j + 0.5) * hStep;
			double multiplier = hStep * hStep;
			double val = daFunc(argX, argY) * ((j != i) ? multiplier : multiplier / 2.0);
			cout << "Client #" << iBegin << ": " << val << endl;
			resultBuf += val;
		}
	}

	//return result
	returnCode = write(sockfd, (void *) &resultBuf, sizeof(double));
	if(returnCode < 0)
		quitWithError("Error writing to socket", 1);

	close(sockfd);
	return 0;
}

/*
*	Function that limits the prism
*/
double daFunc(double x, double y) {
	return x*x + y;
}

/**
*	To avoid code repetition we'll define this little func
*/
void quitWithError(string errMessage, int exitCode) {
	cerr << errMessage << endl;
	cerr << "Exit code: " << exitCode << endl;
	exit(exitCode);
}