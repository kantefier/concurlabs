#include <iostream>
#include <stdlib.h>
#include "mpi.h"
using namespace std;

double daFunc(double x, double y);
void quitWithError(string errMessage, int exitCode);

/**
*	Program entry point.
*	Arguments:
*		~ argv[1]: number of fragments
*	Example:
*		./myprog 6
*/
int main(int argc, char *argv[]) {
	int numprocs, rank;
	MPI_Init(&argc, &argv);

	MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	if(argc == 2) {
		int iBegin = rank;
	    int shift = numprocs;
		int fragmentCount = atoi(argv[1]);
	    double sideLength = 1.0;
		double hStep = sideLength / fragmentCount;

		double finalResult = 0.0;
		double localResult = 0.0;

		cout << scientific;

		for(int i = iBegin; i < fragmentCount; i += shift) {
			for(int j = 0; j <= i; ++j) {
				double argX = (fragmentCount - i - 0.5) * hStep;
				double argY = (j + 0.5) * hStep;
				double multiplier = hStep * hStep;
				double val = daFunc(argX, argY) * ((j != i) ? multiplier : multiplier / 2.0);
				cout << "Process #" << rank << ": " << val << endl;
				localResult += val;
			}
		}
		MPI_Reduce(&localResult, &finalResult, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
		if(rank == 0)
			cout << "Result: " << finalResult << endl;
	} else {
		quitWithError("Not enough arguments.", 1);
	}

	MPI_Finalize();
	return 0;
}

/*
*	Function that limits the prism
*/
double daFunc(double x, double y) {
	return x*x + y;
}

/**
*	To avoid code repetition we'll define this little func
*/
void quitWithError(string errMessage, int exitCode) {
	cerr << errMessage << endl;
	cerr << "Exit code: " << exitCode << endl;
	exit(exitCode);
}