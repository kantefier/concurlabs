#include "DoubleMatrix.h"
#include <iostream>
#include <iomanip>
#include <string.h>
#include <stdexcept>

DoubleMatrix::DoubleMatrix() {
	rowcount = 0;
	colcount = 0;
	dataArr = NULL;
}

DoubleMatrix::DoubleMatrix(int rows, int cols) {
	rowcount = rows;
	colcount = cols;
	dataArr = new double[rowcount * colcount]();
}

DoubleMatrix::DoubleMatrix(const DoubleMatrix &that) {
	rowcount = that.rowcount;
	colcount = that.colcount;
	dataArr = new double[rowcount * colcount]();
	std::copy(&that.dataArr[0], &that.dataArr[rowcount * colcount - 1], dataArr);
	// for(int i = 0; i < rowcount * colcount; ++i)
	// 	dataArr[i] = that(i / colcount, i % colcount);
}

double& DoubleMatrix::operator()(int i, int j) {
	return dataArr[i*colcount + j];
}

DoubleMatrix DoubleMatrix::operator*(DoubleMatrix &that) {
	int resultRowCount = this->rowcount;
	int resultColCount = that.colcount;
	DoubleMatrix resultMatr = DoubleMatrix(resultRowCount, resultColCount);
	for(int i = 0; i < resultRowCount; ++i) {
		for(int j = 0; j < resultColCount; ++j) {
			double resultAcc = 0.0;
			for(int k = 0; k < this->colcount; ++k) {
				resultAcc += (*this)(i, k) * that(k, j);
			}
			resultMatr(i, j) = resultAcc;
		}
	}
	return resultMatr;
}

DoubleMatrix& DoubleMatrix::operator=(const DoubleMatrix &that) {
	rowcount = that.rowcount;
	colcount = that.colcount;
	delete(this->dataArr);
	this->dataArr = new double[rowcount * colcount]();
	std::copy(&that.dataArr[0], &that.dataArr[rowcount * colcount - 1], dataArr);
	return *this;
}

DoubleMatrix DoubleMatrix::concatHorizontal(DoubleMatrix &leftMatr, DoubleMatrix &rightMatr) {
	//check for right input
	if(leftMatr.getRowCount() != rightMatr.getRowCount()) {
		throw std::invalid_argument("Invalid Argument! Concatenating Matrixes rowcount should match!");
	}

	DoubleMatrix resultM = DoubleMatrix(leftMatr.getRowCount(), leftMatr.getColCount() + rightMatr.getColCount());
	int jBorder = leftMatr.getColCount();
	for(int i = 0; i < resultM.getRowCount(); ++i) {
		for(int j = 0; j < resultM.getColCount(); ++j) {
			//in bounds of left or right matrix
			if(j < jBorder) {
				resultM(i, j) = leftMatr(i, j);
			} else {
				resultM(i, j) = rightMatr(i, j - jBorder);
			}
		}
	}
	return resultM;
}

DoubleMatrix DoubleMatrix::concatVertical(DoubleMatrix &leftMatr, DoubleMatrix &rightMatr) {
	//check for right input
	if(leftMatr.getColCount() != rightMatr.getColCount()) {
		throw std::invalid_argument("Invalid Argument! Concatenating Matrixes colcount should match!");
	}

	DoubleMatrix resultM = DoubleMatrix(leftMatr.getRowCount() + rightMatr.getRowCount(), leftMatr.getColCount());
	int iBorder = leftMatr.getRowCount();
	for(int i = 0; i < resultM.getRowCount(); ++i) {
		for(int j = 0; j < resultM.getColCount(); ++j) {
			//in bounds of left or right matrix
			if(i < iBorder) {
				resultM(i, j) = leftMatr(i, j);
			} else {
				resultM(i, j) = rightMatr(i - iBorder, j);
			}
		}
	}
	return resultM;
}

int DoubleMatrix::getRowCount() {
	return rowcount;
}

int DoubleMatrix::getColCount() {
	return colcount;
}

void DoubleMatrix::display() {
	std::cout << std::scientific;
	for(int i = 0; i < rowcount * colcount; ++i) {
		std::cout << std::setw(13) << dataArr[i] << " ";
		if(i != 0 && (i + 1) % colcount == 0)
			std::cout << std::endl;
	}
}

DoubleMatrix::~DoubleMatrix() {
	delete(dataArr);
}