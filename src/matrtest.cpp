#include <iostream>
#include "DoubleMatrix.h"
using namespace std;

int main(int argc, char *argv[]) {
	cout << "Welcome to testing suite for DoubleMatrix class!" << endl;

	int arows = 2;
	int acols = 3;
	DoubleMatrix aMatr = DoubleMatrix(arows, acols);
	aMatr(0,0) = 1;
	aMatr(0,1) = 2;
	aMatr(0,2) = 3;
	aMatr(1,0) = 4;
	aMatr(1,1) = 5;
	aMatr(1,2) = 6;
	aMatr.display();
	cout << endl;

	int brows = 3;
	int bcols = 2;
	DoubleMatrix bMatr = DoubleMatrix(brows, bcols);
	bMatr(0,0) = 7;
	bMatr(0,1) = 10;
	bMatr(1,0) = 8;
	bMatr(1,1) = 11;
	bMatr(2,0) = 9;
	bMatr(2,1) = 12;
	bMatr.display();
	cout << endl;

	DoubleMatrix cMatr = aMatr * bMatr;
	cMatr.display();
	return 1;
}