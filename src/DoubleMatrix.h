class DoubleMatrix {
private:
	double *dataArr;
	int rowcount, colcount;
public:
	DoubleMatrix();
	DoubleMatrix(int, int);
	DoubleMatrix(const DoubleMatrix &that);
	double& operator()(int, int);
	DoubleMatrix operator*(DoubleMatrix &that);
	DoubleMatrix& operator=(const DoubleMatrix &that);
	static DoubleMatrix concatHorizontal(DoubleMatrix &leftMatr, DoubleMatrix &rightMatr);
	static DoubleMatrix concatVertical(DoubleMatrix &leftMatr, DoubleMatrix &rightMatr);
	int getRowCount();
	int getColCount();
	void display();
	~DoubleMatrix();
};